// geo contains geometric types fo Okomoi
package geo

type Point struct {
	X float32
	Y float32
}

type Size Point

func (s Size) Shrink(shrink float32) Size {
	return Size{X: s.X - shrink, Y: s.Y - shrink}
}

func (s Size) Grow(grow float32) Size {
	return Size{X: s.X + grow, Y: s.Y + grow}
}

type At Point

func (a At) Shift(by float32) At {
	return At{X: a.X + by, Y: a.Y + by}
}

type Bounds struct {
	At
	Size
}

func (b Bounds) IsInside(x, y float32) bool {
	return (x >= b.At.X) && (x <= b.At.X+b.Size.X) &&
		(y >= b.At.Y) && (y <= (b.At.Y + b.Size.Y))
}

func NewBounds(x, y, w, h float32) *Bounds {
	return &Bounds{
		At:   At{x, y},
		Size: Size{w, h},
	}
}
