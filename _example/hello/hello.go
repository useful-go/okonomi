package main

import (
	"errors"
	"image/color"
	"log"
)

import (
	"github.com/hajimehoshi/ebiten/v2"
)

import (
	"gitlab.com/useful-go/okonomi/driver/ebidriver"
	"gitlab.com/useful-go/okonomi/event"
	"gitlab.com/useful-go/okonomi/geo"
	_ "gitlab.com/useful-go/okonomi/graphic"
	"gitlab.com/useful-go/okonomi/style"
	"gitlab.com/useful-go/okonomi/widget"
)

type App struct {
	// Top level widget.
	Root    *widget.Root
	Stamp   float64
	Handler event.Handler
	Hello   *widget.Label
	Button  *widget.Button
}

// Update proceeds the state.
// Update is called every tick (1/60 [s] by default).
func (g *App) Update() error {
	g.Stamp++

	// Update UI state.
	done := ebidriver.SendEvents(g.Handler.In(), g.Stamp)
	if done {
		return errors.New("closed")
	}

	// Write your game's logical update.
	return nil
}

// Draw draws the screen.
// Draw is called every frame (typically 1/60[s] for 60Hz display).
func (g *App) Draw(screen *ebiten.Image) {
	// Write your game's rendering.
	screen.Fill(color.RGBA{0x88, 0x99, 0xAA, 0xFF})
	target := ebidriver.NewTarget(screen, false)
	g.Root.Render(target)
}

// Layout takes the outside size (e.g., the window size) and returns the (logical) screen size.
// If you don't have to adjust the screen size with the outside size, just return a fixed size.
func (g *App) Layout(outsideWidth, outsideHeight int) (screenWidth, screenHeight int) {
	return 640, 480
}

func (g *App) HandleUI() {
	select {
	case ev := <-g.Button.Handler().Out():
		log.Printf("button clicked: ", ev)
	}
}

func main() {
	app := &App{}
	app.Root = widget.NewRoot(geo.NewBounds(10, 10, 600, 400), style.NewBasicTheme())
	app.Handler = app.Root.Handler()
	app.Hello = widget.NewLabel("Hello! 今日は！", geo.NewBounds(15, 20, 100, 40))
	app.Button = widget.NewButton("Click me!", "button_1", geo.NewBounds(15, 40, 100, 60))
	app.Root.AddChild(app.Hello)
	app.Root.AddChild(app.Button)
	widget.Debug = true
	go app.HandleUI()

	ebiten.SetWindowSize(640, 480)
	ebiten.SetWindowTitle("hello")
	ebiten.SetWindowClosingHandled(true)
	if err := ebiten.RunGame(app); err != nil {
		log.Fatal(err)
	}
}
