package widget

import (
	"log"
)

import (
	"gitlab.com/useful-go/okonomi/event"
	"gitlab.com/useful-go/okonomi/geo"
	"gitlab.com/useful-go/okonomi/graphic"
	// "gitlab.com/useful-go/okonomi/style"
)

type Button struct {
	*Label
	Action string
}

/** Handles a mouse click event and pass it on.  */
func (bu *Button) Press(at geo.At) {
	if bu.IsInside(at) {
		log.Print("button clicked")
		go func() {
			bu.Handler().Out() <- event.Action{Name: bu.Action}
		}()
	}
}

func (bu *Button) Process(ev event.Event) {
	switch tev := ev.(type) {
	case event.Click:
		if tev.Press {
			bu.Press(tev.At)
		}
	default:
	}
}

func (bu *Button) Render(t graphic.Target) {
	bu.Label.Basic.Render(t)
	bu.Label.Render(t)
}

func NewButton(text, action string, bounds *geo.Bounds) *Button {
	but := &Button{}
	but.Label = NewLabel(text, bounds)
	but.Action = action
	but.Widget = but
	go but.Run()
	return but
}
