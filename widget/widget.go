package widget

import (
	"gitlab.com/useful-go/okonomi/event"
	"gitlab.com/useful-go/okonomi/geo"
	"gitlab.com/useful-go/okonomi/graphic"
	"gitlab.com/useful-go/okonomi/style"
)

/* Cursor is the mouse, keyboard or stick cursor. */
type Cursor struct {
	geo.At
	Hover Widget
	Focus Widget
	style.Style
}

/* Cursors groups the three cursors together */
type Cursors struct {
	Mouse    Cursor
	Keyboard Cursor
	Stick    Cursor
}

/* Widget is the interface to widgets.
   All methods are read-only, to change the state of te widget send
   events to the In channel of the Handler.
*/
type Widget interface {
	Bounds() geo.Bounds
	Style() style.Style
	Parent() Widget
	Children() []Widget
	Root() RootWidget
	Render(graphic.Target)
	Flags() Flags
	Handler() event.Handler
	Run()
	Process(event.Event)
}

/* RootWidget is the interface the root widget implements */
type RootWidget interface {
	Widget
	Cursors() Cursors
	Theme() style.Theme
}
