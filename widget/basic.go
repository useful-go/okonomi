package widget

import "log"

import (
	"gitlab.com/useful-go/okonomi/event"
	"gitlab.com/useful-go/okonomi/geo"
	"gitlab.com/useful-go/okonomi/graphic"
	"gitlab.com/useful-go/okonomi/style"
)

var Debug = false

type Flags struct {
	Hide    bool
	Disable bool
	Hover   bool
	Marked  bool
	Close   bool
}

type State struct {
	Root     RootWidget
	Size     geo.Size
	Bounds   geo.Bounds
	Parent   Widget
	Children []Widget
	Style    style.Style
	Flags
}

type BasicHandler struct {
	InChan    chan event.Event
	OutChan   chan event.Event
	ChildChan chan event.Event
	DrawChan  chan graphic.Render
}

func (h *BasicHandler) Setup() {
	h.InChan = make(chan event.Event)
	h.OutChan = make(chan event.Event)
	h.ChildChan = make(chan event.Event)
	h.DrawChan = make(chan graphic.Render)
}

func (h *BasicHandler) In() chan event.Event {
	return h.InChan
}
func (h *BasicHandler) Out() chan event.Event {
	return h.OutChan
}

func (h *BasicHandler) Draw() chan graphic.Render {
	return h.DrawChan
}

func (w *Basic) Process(ev event.Event) {
	if Debug {
		log.Printf("event: %T: %v", w.Widget, ev)
	}
	switch tev := ev.(type) {
	case event.Mouse:
		w.State.Flags.Hover = w.IsInside(tev.At)
	}
}

// NotifyChildren forwards all events to the enabled child widgets,
// which should decide what to do with them by themselves.
func (w *Basic) NotifyChildren(ev event.Event) {
	for _, child := range w.State.Children {
		// if !child.Flags().Disable {
		child.Handler().In() <- ev
		// }
	}
}

func (w *Basic) Run() {
	for !w.State.Flags.Close {
		select {
		case ev := <-w.Handler().In():
			if ev == nil {
				w.State.Flags.Close = true
			} else {
				if w.Widget != nil {
					w.Widget.Process(ev)
				} else {
					w.Process(ev)
				}
				w.NotifyChildren(ev)
			}
		}
	}
}

type Basic struct {
	// Widget is the widget we virually inherit from.
	// It should be set to the actual widget implementation.
	Widget
	State
	BasicHandler
}

var _ Widget = &Basic{}

func (b Basic) At() geo.At {
	return b.State.Bounds.At
}

func (b Basic) Size() geo.Size {
	return b.State.Bounds.Size
}

func (b Basic) Bounds() geo.Bounds {
	return b.State.Bounds
}

func (b Basic) Flags() Flags {
	return b.State.Flags
}

func (b Basic) Style() style.Style {
	return b.State.Style
}

func (b Basic) Parent() Widget {
	return b.State.Parent
}

func (b Basic) Children() []Widget {
	return b.State.Children
}

func (b Basic) Root() RootWidget {
	return b.State.Root
}

var render int

func (b *Basic) Render(t graphic.Target) {
	bg := b.Style().Background.Color
	bo := b.Style().Border.Color
	if b.Flags().Hover {
		bg = b.Style().State.Hover
	}
	t.Rectangle(b.At(), b.Size(), bg)
	t.Box(b.At(), b.Size(), b.Style().Border.Size, bo)
	for _, child := range b.State.Children {
		if !child.Flags().Hide {
			child.Render(t)
		}
	}
}

func (b *Basic) Handler() event.Handler {
	return &b.BasicHandler
}

func (b *Basic) SetAt(at geo.At) {
	b.State.Bounds.At = at
}

func (b *Basic) SetSize(size geo.Size) {
	b.State.Size = size
}

func (b *Basic) SetBounds(bounds geo.Bounds) {
	b.State.Bounds = bounds
}

func NewBasicWithStyle(parent Widget, bounds *geo.Bounds, styl *style.Style) *Basic {
	b := &Basic{}
	b.Widget = b

	if parent != nil {
		b.State.Root = parent.Root()
		b.State.Style = parent.Style()

		if root, ok := parent.(RootWidget); ok {
			b.State.Root = root
		} else {
			b.State.Root = parent.Root()
		}

		b.State.Parent = parent
		b.SetBounds(parent.Bounds())
	} else {
		b.State.Style = style.DefaultStyle
	}

	if bounds != nil {
		b.SetBounds(*bounds)
	}

	if styl != nil {
		b.State.Style = *styl
	}

	b.BasicHandler.Setup()

	return b
}

func NewBasic(parent Widget, bounds *geo.Bounds) *Basic {
	return NewBasicWithStyle(parent, bounds, nil)
}

func (b *Basic) AddChild(widget Widget) Widget {
	b.State.Children = append(b.State.Children, widget)
	return widget
}

// Checks if the coordinates are inside the main rectange of the widget.
func (bw *Basic) IsInside(at geo.At) bool {
	return bw.State.Bounds.IsInside(at.X, at.Y)
}
