package widget

import (
	// "gitlab.com/useful-go/okonomi/event"
	"gitlab.com/useful-go/okonomi/geo"
	"gitlab.com/useful-go/okonomi/graphic"
	"gitlab.com/useful-go/okonomi/style"
)

/* Caption is a string that can be rendered as a caption */
type Caption string

func (c Caption) Render(t graphic.Target, at geo.At, s style.Style) {
	face := s.Foreground.Face
	colo := s.Foreground.Color
	lh := float32(face.Metrics().Height.Round())
	at.X += lh
	at.Y += s.Foreground.Margin
	t.Print(at, string(c), face, colo)
}

// Label widget.
type Label struct {
	*Basic
	Caption
}

func (l Label) Render(t graphic.Target) {
	l.Caption.Render(t, l.State.Bounds.At, l.State.Style)
}

func NewLabel(text string, bounds *geo.Bounds) *Label {
	res := &Label{Caption: Caption(text)}
	res.Basic = NewBasicWithStyle(nil, bounds, nil)
	res.Basic.Widget = res
	go res.Run()
	return res
}
