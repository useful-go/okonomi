package widget

import (
	"gitlab.com/useful-go/okonomi/geo"
	"gitlab.com/useful-go/okonomi/style"
)

// Root level widget, can be used as desktop.
type Root struct {
	*Basic
	UITheme style.Theme
	Curs    Cursors
}

func (r Root) Cursors() Cursors {
	return r.Curs
}

func (r Root) Theme() style.Theme {
	return r.UITheme
}

const RootStyle = "root"

func NewRoot(bounds *geo.Bounds, theme style.Theme) *Root {
	root := &Root{}
	style := theme.StyleFor(RootStyle)
	root.Basic = NewBasicWithStyle(nil, bounds, &style)
	root.State.Root = root // Root widget is self-rooted.
	root.Basic.Widget = root
	root.UITheme = theme
	go root.Run()
	return root
}

var _ RootWidget = &Root{}
