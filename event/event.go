/* event are events for Okonomi. */
package event

import (
	"gitlab.com/useful-go/okonomi/geo"
	"gitlab.com/useful-go/okonomi/graphic"
)

type Event interface {
	Timestamp() float64
}

type Basic struct {
	Stamp     float64
	Name      string
	EventData any
}

func (be Basic) Data() interface{} {
	return be.EventData
}

func (be Basic) Timestamp() float64 {
	return be.Stamp
}

type Action struct {
	Basic
	Name string
}

/* Update event, when UI has to update (animations, etc). */
type Update struct {
	Basic
	TimePassed float64
}

/* Resize event, when the parent of an element has resized. */
type Resize struct {
	Basic
	NewSize geo.Bounds
}

/* Cleanup event. */
type Destroy struct {
	Basic
}

/* Close event. */
type Close struct {
	Basic
	Parent any
}

/* New child event. */
type NewChild struct {
	Basic
	Child any
}

type Widget struct {
	Basic
	Widget any
}

type Axis struct {
	Basic
	ID    int
	Stick int
	Axis  int
	Pos   float64
}

type Button struct {
	Basic
	ID     int
	Button int
	Press  bool
}

type Click struct {
	Basic
	geo.At
	Button   int
	Press    bool
	Release  bool
	Duration int
}

func (c Click) Position() geo.At {
	return c.At
}

type Tap struct {
	Basic
	geo.At
	ID int
}

func (c Tap) Position() geo.At {
	return c.At
}

type Touch struct {
	Basic
	geo.At
	ID       int
	Press    bool
	Duration int
}

func (c Touch) Position() geo.At {
	return c.At
}

type Key struct {
	Basic
	KeyCode int
	Text    string
	Press   bool
	Release bool
}

type Text struct {
	Basic
	Runes []rune
}

type Mouse struct {
	Basic
	geo.At
	Delta geo.Size
}

func (c Mouse) Position() geo.At {
	return c.At
}

type Wheel struct {
	Basic
	geo.At
	Size geo.Size
}

type Handler interface {
	In() chan Event
	Out() chan Event
	Draw() chan graphic.Render
}

type HasPosition interface {
	Position() geo.At
}
