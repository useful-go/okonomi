module gitlab.com/useful-go/okonomi

go 1.18

require (
	github.com/ebitengine/purego v0.3.0 // indirect
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20221017161538-93cebf72946b // indirect
	github.com/hajimehoshi/bitmapfont/v3 v3.0.0 // indirect
	github.com/hajimehoshi/ebiten/v2 v2.5.5 // indirect
	github.com/jezek/xgb v1.1.0 // indirect
	github.com/srwiley/oksvg v0.0.0-20221011165216-be6e8873101c // indirect
	github.com/srwiley/rasterx v0.0.0-20210519020934-456a8d69b780 // indirect
	golang.org/x/exp v0.0.0-20230626212559-97b1e661b5df // indirect
	golang.org/x/exp/shiny v0.0.0-20230626212559-97b1e661b5df // indirect
	golang.org/x/image v0.9.0 // indirect
	golang.org/x/mobile v0.0.0-20230301163155-e0f57694e12c // indirect
	golang.org/x/net v0.6.0 // indirect
	golang.org/x/sync v0.1.0 // indirect
	golang.org/x/sys v0.6.0 // indirect
	golang.org/x/text v0.11.0 // indirect
)
