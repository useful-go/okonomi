package style

import (
	"image/color"
)

import (
	"github.com/hajimehoshi/bitmapfont/v3"
	"gitlab.com/useful-go/okonomi/graphic"
)

type Border struct {
	Size    float32
	Color   graphic.Color
	Shadow  graphic.Color
	Padding float32
}

type Background struct {
	Color  graphic.Color
	Bitmap graphic.Bitmap
}

type Foreground struct {
	Color  graphic.Color
	Bitmap graphic.Bitmap
	Face   graphic.Face
	Size   float32
	Margin float32
}

type State struct {
	Disable graphic.Color
	Error   graphic.Color
	Focus   graphic.Color
	Press   graphic.Color
	Select  graphic.Color
	Hover   graphic.Color
}

// A style detemines the style of widget.
type Style struct {
	Foreground Foreground
	Background Background
	Border     Border
	State      State
}

/*
	Theme is a set of style elements for the whole UI.
*/
type Theme interface {
	// StyleFor shouldreturn the style to use for the
	// named UI element, or if not known, return the default style.
	StyleFor(name string) Style
}

type BasicTheme struct {
	Default Style
	Styles  map[string]Style
}

var DefaultStyle = Style{
	Foreground: Foreground{
		Face:   bitmapfont.Face,
		Color:  color.Black,
		Margin: 3,
		Size:   12,
	}, Background: Background{
		Color: color.RGBA{0xbb, 0xbb, 0xbb, 255},
	}, Border: Border{
		Size:   1,
		Color:  color.RGBA{128, 128, 128, 255},
		Shadow: color.RGBA{0, 0, 0, 128},
	}, State: State{
		Hover: color.RGBA{0xcc, 0xcc, 0xcc, 255},
	},
}

func NewBasicTheme() *BasicTheme {
	return &BasicTheme{
		Default: DefaultStyle,
		Styles:  make(map[string]Style),
	}
}

func (b BasicTheme) StyleFor(name string) Style {
	style, ok := b.Styles[name]
	if ok {
		return style
	}
	return b.Default
}

// button
// caption
// cancel
// checkbox
// input
// pressed
// radio
// scrollbar
// separator
