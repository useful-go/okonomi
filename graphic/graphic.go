// graphic is an abstract graphic package for okonomi
package graphic

import "image"
import "image/color"
import "golang.org/x/image/font"

import (
	"gitlab.com/useful-go/okonomi/geo"
)

type Image = image.Image
type Color = color.Color
type Face = font.Face
type SVG []byte

// Bitmap is a bitmap image.
type Bitmap interface {
	Size() geo.Size
	Target() Target
}

// Vector is a vector image.
type Vector interface {
	Size() geo.Size
	Arc(at geo.At, radius, startAngle, endAngle float32, clockwise bool)
	ArcTo(at geo.At, size geo.Size, radius float32)
	Close()
	CubicTo(at geo.At, c1, c2 geo.At)
	LineTo(at geo.At)
	MoveTo(at geo.At)
	QuadTo(at geo.At, control geo.At)
}

// Target is a target for drawing.
type Target interface {
	Antialias(b bool)
	Size() geo.Size
	Print(at geo.At, text string, face Face, clr Color)
	Disk(at geo.At, r float32, clr Color)
	Rectangle(at geo.At, size geo.Size, clr Color)
	Circle(at geo.At, r float32, strokeWidth float32, clr Color)
	Line(at geo.At, size geo.Size, strokeWidth float32, clr Color)
	Box(at geo.At, size geo.Size, strokeWidth float32, clr Color)
	Bitmap(at geo.At, bmp Bitmap, theta *float64, scale *geo.Size, skew *geo.Size)
}

// Converteer converts from general types to target specific types.
type Converter interface {
	BitmapFromImage(image Image) (Bitmap, error)
	Bitmap(size geo.Size) (Bitmap, error)
}

type VectorConverter interface {
	Converter
	VectorFromSVG(svg SVG, defaultSize geo.Size) (Vector, error)
	Vector(defaultSize geo.Size) (Vector, error)
}

// Font is a type of lettering. It has one or more faces which are size specific.
type Font interface {
	// Face returns a face ofthe font with the given size.
	// If it is a bitmap font the size may be ignored.
	Face(size float32) (Face, error)
}

// A graphical platform consists of a converter and a screen target.
type Platform interface {
	Converter() Converter
	Screen() Target
}

// Render is a single rendering command to be sent to the renderer.
type Render = func(Target)
