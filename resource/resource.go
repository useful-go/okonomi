package resource

import (
	"bytes"
	"errors"
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"io"
	"io/fs"
	"path"
)

import (
	"github.com/hajimehoshi/bitmapfont/v3"
	"github.com/srwiley/oksvg"
	"github.com/srwiley/rasterx"
	"golang.org/x/image/font"
	"golang.org/x/image/font/opentype"
)

import (
	"gitlab.com/useful-go/okonomi/graphic"
)

/*	tt, err := opentype.Parse(fonts.MPlus1pRegular_ttf)
	if err != nil {
		log.Fatal(err)
	}

	const dpi = 72
	mplusNormalFont, err = opentype.NewFace(tt, &opentype.FaceOptions{
		Size:    24,
		DPI:     dpi,
		Hinting: font.HintingVertical,
	})
	if err != nil {
		log.Fatal(err)
	}
	mplusBigFont, err = opentype.NewFace(tt, &opentype.FaceOptions{
		Size:    48,
		DPI:     dpi,
		Hinting: font.HintingFull, // Use quantization to save glyph cache images.
	})
	if err != nil {
		log.Fatal(err)
	}

)*/

type Face struct {
	Size float32
	font.Face
}

type OpentypeFont struct {
	*opentype.Font
	Faces []Face
	DPI   int
}

func NewOpentypeFont(f *opentype.Font, dpi int) *OpentypeFont {
	return &OpentypeFont{Font: f, DPI: dpi}
}

func (f *OpentypeFont) Face(size float32) (graphic.Face, error) {
	var err error
	// Dumb linear search, use x/exp/slices later...
	for _, face := range f.Faces {
		if face.Size == size {
			return face, nil
		}
	}

	// not found
	if f.Font == nil {
		// Fallback to face even if the size is wrong.
		if len(f.Faces) > 1 {
			return f.Faces[0], nil
		}
		return nil, errors.New("No font available")
	}

	face := Face{Size: size}
	face.Face, err = opentype.NewFace(f.Font, &opentype.FaceOptions{
		Size:    float64(size),
		DPI:     float64(f.DPI),
		Hinting: font.HintingFull,
	})
	if err != nil {
		return nil, err
	}
	f.Faces = append(f.Faces, face)
	return face, nil
}

type DefaultFont struct {
	font.Face
}

type Resource any
type ResourceMap map[string]Resource
type Loader func(rd io.Reader) (Resource, error)
type LoaderMap map[string]Loader

type Builtins struct {
	Face
}

var Builtin = Builtins{
	Face: Face{
		Size: 12,
		Face: bitmapfont.Face,
	},
}

type Manager struct {
	fs.FS
	graphic.Converter
	DPI       int
	Resources ResourceMap
	Loaders   LoaderMap
}

const DefaultDPI = 72

func NewManager(fs fs.FS) *Manager {
	m := &Manager{}
	m.FS = fs
	m.DPI = DefaultDPI
	m.Resources = ResourceMap{}
	m.Loaders = LoaderMap{}
	m.DefaultLoaders()
	return m
}

func (m *Manager) DefaultLoaders() {
	m.Loaders[".otf"] = m.LoadOTF
	m.Loaders[".ttf"] = m.LoadOTF
	m.Loaders[".png"] = m.LoadPNG
	m.Loaders[".jpg"] = m.LoadJPG
	m.Loaders[".jpeg"] = m.LoadJPG
	m.Loaders[".svg"] = m.LoadSVG
}

func (m *Manager) LoadWithLoader(name string, load Loader) (Resource, error) {
	res, ok := m.Resources[name]
	if ok {
		return res, nil
	}
	f, err := m.FS.Open(name)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	res, err = load(f)
	if err != nil {
		return nil, err
	}
	m.Resources[name] = res
	return res, nil
}

func (m *Manager) LoadOTF(rd io.Reader) (Resource, error) {
	buf := bytes.NewBuffer([]byte{})
	_, err := io.Copy(buf, rd)
	if err != nil {
		return nil, err
	}
	reader := bytes.NewReader(buf.Bytes())
	// Do something with `reader`

	tt, err := opentype.ParseReaderAt(reader)
	if err != nil {
		return nil, err
	}
	return NewOpentypeFont(tt, m.DPI), nil
}

func (m *Manager) LoadPNG(rd io.Reader) (Resource, error) {
	img, err := png.Decode(rd)
	if err != nil {
		return nil, err
	}
	if m.Converter != nil {
		return m.Converter.BitmapFromImage(img)
	}
	return img, nil
}

func (m *Manager) LoadJPG(rd io.Reader) (Resource, error) {
	img, err := jpeg.Decode(rd)
	if err != nil {
		return nil, err
	}
	if m.Converter != nil {
		return m.Converter.BitmapFromImage(img)
	}
	return img, nil
}

func (m *Manager) LoadGIF(rd io.Reader) (Resource, error) {
	img, err := gif.Decode(rd)
	if err != nil {
		return nil, err
	}
	if m.Converter != nil {
		return m.Converter.BitmapFromImage(img)
	}
	return img, nil
}

func (m *Manager) LoadSVG(rd io.Reader) (Resource, error) {
	var w, h float64 = 64, 64

	icon, err := oksvg.ReadIconStream(rd)
	if err != nil {
		return nil, err
	}
	if icon.ViewBox.H > 0 {
		h = icon.ViewBox.H
	}
	if icon.ViewBox.W > 0 {
		w = icon.ViewBox.W
	}

	icon.SetTarget(0, 0, w, h)
	iw, ih := int(w), int(h)
	rgba := image.NewRGBA(image.Rect(0, 0, iw, ih))
	scan := rasterx.NewScannerGV(iw, ih, rgba, rgba.Bounds())
	icon.Draw(rasterx.NewDasher(iw, ih, scan), 1)
	if m.Converter != nil {
		return m.Converter.BitmapFromImage(rgba)
	}
	return rgba, nil
}

// Load loads the resource with the given name, caching it on load.
// If the resource was already loaded, the cached resource is returned.
// The loader is determined by the file extension and the Loaders of the
// manager.
func (m *Manager) Load(name string) (Resource, error) {
	res, ok := m.Resources[name]
	if ok {
		return res, nil
	}

	ext := path.Ext(name)
	loader, ok := m.Loaders[ext]
	if !ok {
		return nil, errors.New("no loader for extension: " + ext)
	}
	return m.LoadWithLoader(name, loader)
}
