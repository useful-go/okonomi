# okonomi

Okonomi is a GUI library for Go. It use Ebiten on the backend, although
this is abstracted. It is a concurrent UI based on go routines.

## Authors and acknowledgment

Copyright bjorndm 2023.

## License

Licensed under the MIT license.

## Project status

Alpha.


