package ebidriver

import (
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
	"github.com/hajimehoshi/ebiten/v2/text"
	"github.com/hajimehoshi/ebiten/v2/vector"
	"golang.org/x/image/font"
)

import (
	"gitlab.com/useful-go/okonomi/event"
	"gitlab.com/useful-go/okonomi/geo"
	"gitlab.com/useful-go/okonomi/graphic"
)

// Bitmap is a bitmap image.
type Bitmap struct {
	*ebiten.Image
	antialias bool
}

func (b Bitmap) Size() geo.Size {
	w, h := b.Image.Size()
	return geo.Size{X: float32(w), Y: float32(h)}
}

func (b *Bitmap) Target() graphic.Target {
	return b
}

func (b *Bitmap) Antialias(aa bool) {
	b.antialias = aa
}

func (b *Bitmap) Print(at geo.At, txt string, face graphic.Face, clr graphic.Color) {
	// the font must in fact be a font.Face
	ff := face.(font.Face)
	text.Draw(b.Image, txt, ff, int(at.X), int(at.Y), clr)
}

func (b *Bitmap) Disk(at geo.At, r float32, clr graphic.Color) {
	vector.DrawFilledCircle(b.Image, at.X, at.Y, r, clr, b.antialias)
}

func (b *Bitmap) Circle(at geo.At, r float32, strokeWidth float32, clr graphic.Color) {
	vector.StrokeCircle(b.Image, at.X, at.Y, r, strokeWidth, clr, b.antialias)
}

func (b *Bitmap) Rectangle(at geo.At, size geo.Size, clr graphic.Color) {
	vector.DrawFilledRect(b.Image, at.X, at.Y, size.X, size.Y, clr, b.antialias)

}

func (b *Bitmap) Box(at geo.At, size geo.Size, strokeWidth float32, clr graphic.Color) {
	vector.StrokeRect(b.Image, at.X, at.Y, size.X, size.Y, strokeWidth, clr, b.antialias)
}

func (b *Bitmap) Line(at geo.At, size geo.Size, strokeWidth float32, clr graphic.Color) {
	x0 := at.X
	y0 := at.Y
	x1 := at.X + size.X
	y1 := at.Y + size.Y
	vector.StrokeLine(b.Image, x0, y0, x1, y1, strokeWidth, clr, b.antialias)
}

func (b *Bitmap) Bitmap(at geo.At, bmp graphic.Bitmap, theta *float64, scale *geo.Size, skew *geo.Size) {
	real := bmp.(*Bitmap)
	opts := &ebiten.DrawImageOptions{}
	if skew != nil {
		opts.GeoM.Skew(float64(skew.X), float64(skew.Y))
	}
	if scale != nil {
		opts.GeoM.Scale(float64(scale.X), float64(scale.Y))
	}
	if theta != nil {
		opts.GeoM.Rotate(*theta)
	}
	opts.GeoM.Translate(float64(at.X), float64(at.Y))
	b.Image.DrawImage(real.Image, opts)
}

// Assert interface implementation
var _ graphic.Bitmap = &Bitmap{}

// Assert interface implementation
var _ graphic.Target = &Bitmap{}

// Converteer converts from general types to ebiten specific types.
type Converter struct {
	Antialias bool
}

func (c Converter) NewBitmap(img *ebiten.Image) *Bitmap {
	return &Bitmap{Image: img, antialias: c.Antialias}
}

func (c Converter) BitmapFromImage(image graphic.Image) (graphic.Bitmap, error) {
	img := ebiten.NewImageFromImage(image)
	return c.NewBitmap(img), nil
}

func (c Converter) Bitmap(size geo.Size) (graphic.Bitmap, error) {
	img := ebiten.NewImage(int(size.X), int(size.Y))
	return c.NewBitmap(img), nil
}

func NewTarget(img *ebiten.Image, antialias bool) graphic.Target {
	return &Bitmap{Image: img, antialias: antialias}
}

var mouseButtons = []ebiten.MouseButton{
	ebiten.MouseButton0,
	ebiten.MouseButton1,
	ebiten.MouseButton2,
	ebiten.MouseButton3,
	ebiten.MouseButton4,
}

// Last known cursor position.
var lastCursor geo.At

// SendEvents sends events to the channel. It should only be called in the
// ebiten Update() function. It is not thread safe, and may not be used
// in any other way.
func SendEvents(in chan event.Event, stamp float64) bool {
	var runes = make([]rune, 0, 128)

	basic := event.Basic{Stamp: stamp}
	basic.Name = "in_text"
	runes = ebiten.AppendInputChars(runes)
	if len(runes) > 0 {
		in <- event.Text{Basic: basic, Runes: runes}
	}

	basic.Name = "in_key_press"
	var keys = make([]ebiten.Key, 0, 4)
	keys = inpututil.AppendJustPressedKeys(keys)
	for _, key := range keys {
		in <- event.Key{Basic: basic,
			KeyCode: int(key),
			Text:    key.String(),
			Press:   true,
		}
	}

	basic.Name = "in_key_down"
	keys = keys[0:0]
	keys = inpututil.AppendPressedKeys(keys)
	for _, key := range keys {
		in <- event.Key{Basic: basic,
			KeyCode: int(key),
			Text:    key.String(),
		}
	}

	basic.Name = "in_key_up"
	keys = keys[0:0]
	keys = inpututil.AppendJustReleasedKeys(keys)
	for _, key := range keys {
		in <- event.Key{Basic: basic,
			KeyCode: int(key),
			Text:    key.String(),
			Release: true,
		}
	}

	basic.Name = "in_touch"
	var touches = make([]ebiten.TouchID, 0, 4)
	touches = ebiten.AppendTouchIDs(touches)
	for _, touch := range touches {
		x, y := ebiten.TouchPosition(touch)
		duration := inpututil.TouchPressDuration(touch)
		release := inpututil.IsTouchJustReleased(touch)
		in <- event.Touch{Basic: basic,
			At:       geo.At{float32(x), float32(y)},
			Duration: duration,
			Press:    !release,
			ID:       int(touch),
		}
	}

	basic.Name = "in_tap"
	var taps = make([]ebiten.TouchID, 0, 4)
	taps = inpututil.AppendJustPressedTouchIDs(taps)
	for _, touch := range taps {
		x, y := ebiten.TouchPosition(touch)
		in <- event.Tap{Basic: basic,
			At: geo.At{float32(x), float32(y)},
			ID: int(touch),
		}
	}

	basic.Name = "in_mouse_pos"
	mouseX, mouseY := ebiten.CursorPosition()
	mouseXF, mouseYF := float32(mouseX), float32(mouseY)
	if mouseXF != lastCursor.X || mouseYF != lastCursor.Y {
		in <- event.Mouse{
			Basic: basic,
			At:    geo.At{mouseXF, mouseYF},
			Delta: geo.Size{mouseXF - lastCursor.X, mouseYF - lastCursor.Y},
		}
	}
	lastCursor.X, lastCursor.Y = mouseXF, mouseYF

	basic.Name = "in_mouse_button"
	for _, button := range mouseButtons {
		press := inpututil.IsMouseButtonJustPressed(button)
		release := inpututil.IsMouseButtonJustReleased(button)
		// now := ebiten.IsMouseButtonPressed(button)
		if !press && !release {
			continue
		}

		duration := inpututil.MouseButtonPressDuration(button)

		in <- event.Click{Basic: basic,
			At:       geo.At{float32(mouseX), float32(mouseY)},
			Duration: duration,
			Press:    press,
			Release:  release,
			Button:   int(button),
		}
	}

	basic.Name = "in_mouse_wheel"
	wheelX, wheelY := ebiten.Wheel()
	if wheelX != 0 || wheelY != 0 {
		in <- event.Wheel{
			Basic: basic,
			At:    geo.At{float32(mouseX), float32(mouseY)},
			Size:  geo.Size{float32(wheelX), float32(wheelY)},
		}
	}

	if ebiten.IsWindowBeingClosed() {
		close(in)
		return true
	}

	return false
}
